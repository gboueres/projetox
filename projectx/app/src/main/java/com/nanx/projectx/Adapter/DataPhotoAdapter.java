package com.nanx.projectx.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nanx.projectx.R;
import com.nanx.projectx.object.Data;

import java.util.List;



public class DataPhotoAdapter extends RecyclerView.Adapter<DataPhotoAdapter.CustomViewHolder> {

    /*
    inicialmente um adapter é um padrao de projeto
    ele é um padrao estrutural, ele basicamente tem a finalidade de converter uma classe em outra
    como se fosse um adaptor de tomada, permitindo tu encaixar algo em outra

    em outras palavras, ela serve como um molde
    no nosso caso nosso objetivo final é apresentar uma imagem com um texto
    mas nosso objeto possui muito mais informaçoes
     */

    private List<Data> dataList;
    //O Contexto é uma maneira de permitir q o aplicativo utiliza recursos do sistema android
    //neste caso nós passamos o contexto pois o glide para inserir a imagem precisa de recursos do sitema
    //o contexto permite diversas coisas, como acesso envio de sms, acesso a sensores, etc
    //caso nao precisamos do glide neste exemplo, o contexto nao seria necessario
    private Context context;

    //nosso construtor da classe, onde recebemos o contexto da aplicacao e nossa lista
    public DataPhotoAdapter(Context context, List<Data> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    //a funcao do viewHolder é agilizar o processo de consulta e vinculacao do findviewbyid
    // em vez de vincular cada xml, ele realiza isto apenas uma vez e guarda elas para reutilizaçao
    //pois estamos trabalhando dentro do nosso adapter, nosso molde, logo elas nao mudam
    class CustomViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public TextView txtTitle;

        public ImageView imageView;

        CustomViewHolder(View itemView) {

            super(itemView);
            mView = itemView;

            txtTitle = mView.findViewById(R.id.textView);
            imageView = mView.findViewById(R.id.imageView);
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflamos nosso layout
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_row, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        //configuramos nossos dados dentro de nosso xml
        holder.txtTitle.setText(dataList.get(position).getTitle());
        Glide.with(context).load(dataList.get(position).getThumbnailUrl()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        //retorna o tamanho de nossa lista
        return dataList.size();
    }

//    public void alteraAlgo()
//    {
//        dataList.remove(0);
//        notifyDataSetChanged();
//    }
}

